

CPPFLAGS = -I/usr/X11R6
CFLAGS = -g -Wall

LDLIBS =  -lpam -lX11
LDFLAGS = -L/usr/X11R6/lib

OBJS = xgone.o auth.o

.PHONY: all
all: xgone

xgone: $(OBJS)

.PHONY: clean
clean:
	rm -f $(OBJS) xgone

xgone.o auth.o: auth.h
