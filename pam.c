#include <stdio.h>
#include <stdlib.h>

#include <security/pam_appl.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

int my_conv(int num_msg, const struct pam_message **msg,
                  struct pam_response **resp, void *appdata_ptr) {
  int i;
  printf("Num messages: %d\n", num_msg);
  for (i = 0; i < num_msg; i++) {
    char *t = "unknown message type"; 
    const struct pam_message * mesg;
    struct pam_response *pr;
    char *pw = appdata_ptr;

    mesg =  msg[i];
   
    switch (mesg->msg_style) {
      case
      PAM_PROMPT_ECHO_OFF :
      t = "PAM_PROMPT_ECHO_OFF";
      break;
      case PAM_PROMPT_ECHO_ON :
      t = "PAM_PROMPT_ECHO_ON";
      break;
      case PAM_ERROR_MSG :
      t = "PAM_ERROR_MSG";
      break;
      case PAM_TEXT_INFO :
      t = "PAM_TEXT_INFO";
      break;
      //case PAM_MSG_NOCONF :
      //t = "PAM_MSG_NOCONF";
      //break;
      //case PAM_CONV_INTERRUPT :
      //t = "PAM_CONV_INTERRUPT";
      //break;
    }

    printf("PAM msg style: %s; msg: %s\n", t, mesg->msg);
    // pam frees this (says solaris man page)
    pr = calloc(sizeof(struct pam_response),1);
    pr->resp = malloc(strlen(pw)+1);
    strcpy(pr->resp, pw);
    
    *resp = pr;

}
  
    return 0;
}

void print_err(int e, struct pam_handle * ph) {
  char *t = "unknown";
  switch (e) {
    case
PAM_SUCCESS:
t = "PAM_SUCCESS";
    break;
    case 
     PAM_OPEN_ERR :
     t = "PAM_OPEN_ERR";
    break;
case 
     PAM_SYMBOL_ERR :
     t = "PAM_SYMBOL_ERR";
break;
case

     PAM_SERVICE_ERR :
t =     "PAM_SERVICE_ERR";
break;
case
     PAM_SYSTEM_ERR :
     t = "PAM_SYSTEM_ERR";
break;
case 
PAM_BUF_ERR :
t = "PAM_BUF_ERR";
break;
case
     PAM_CONV_ERR:
     t = "PAM_CONV_ERR";
break;
case 
     PAM_PERM_DENIED :
     t = "PAM_PERM_DENIED";
break;
// authenticate
case 
PAM_AUTH_ERR :
t = "PAM_AUTH_ERR";
break;
case
PAM_CRED_INSUFFICIENT :
t = "PAM_CRED_INSUFFICIENT";
break;
case 
PAM_AUTHINFO_UNAVAIL :
t = "PAM_AUTHINFO_UNAVAIL";
break;
case
PAM_USER_UNKNOWN :
t = "PAM_USER_UNKNOWN";
break;
case
PAM_MAXTRIES :
t = "PAM_MAXTRIES";
break;
case
  PAM_NEW_AUTHTOK_REQD :
  t = "PAM_NEW_AUTHTOK_REQD";
break;
case 
  PAM_ACCT_EXPIRED :
  t = "PAM_ACCT_EXPIRED";
break;
  }
  printf("PAM return: %s\n", t);
  //perror(NULL);
  printf("pam error: %s\n", pam_strerror(ph, e));
}

int main(int argc, char **argv)
{
  int e;
  char *user_name;
  char hostname[200];
  struct passwd *pw;
  struct pam_conv *pc;
  struct pam_handle *ph;
  char *p, *t;

  char *service = "login";

  if (argc > 1)
    service = argv[1];

  pc = calloc(sizeof(struct pam_conv), 1);
  pc->conv = my_conv;

  pw = getpwuid(getuid());
  user_name = pw->pw_name;
  printf("Username: %s\n", user_name);

  t = getpass("Enter user password: ");
  p = malloc(strlen(t)+1);
  strcpy(p, t);
  pc->appdata_ptr = p;
  
  printf("Service: %s\n", service);
  
  e = pam_start(service, user_name, pc, &ph);
  print_err(e, ph);
  gethostname(hostname, sizeof(hostname));
/*  e = pam_set_item(ph, PAM_RHOST, hostname);
  print_err(e, ph); */
/*  e = pam_set_item(ph, PAM_RUSER, user_name);
  print_err(e, ph); */
  printf("authenticate\n");
  e = pam_authenticate(ph, 0);
  print_err(e, ph);
  printf("acct_mgmt\n");
  e = pam_acct_mgmt(ph, 0);
  print_err(e, ph);
  e = pam_end(ph, 0);
  print_err(e, ph);

  return 0;
}
