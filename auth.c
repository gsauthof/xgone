#include "auth.h"

#include <stdlib.h>
#include <string.h>

#include <security/pam_appl.h>

static int conv(int num_msg, const struct pam_message **msg,
                  struct pam_response **resp, void *appdata_ptr)
{
  char *pw = appdata_ptr;
  int i;

  for (i = 0; i < num_msg; i++) {
    const struct pam_message *mesg;
    struct pam_response *pr;

    mesg =  msg[i];
  
    if (mesg->msg_style == PAM_PROMPT_ECHO_OFF) {
      pr = calloc(sizeof(struct pam_response), 1);
      pr->resp = malloc(strlen(pw) + 1);
      strcpy(pr->resp, pw);
      *resp = pr;
      return 0;
    }
  }
  return -1; 
}

int authenticate(char *user, char *pw)
{
  int e;
  struct pam_conv *pc;
  struct pam_handle *ph;

  pc = calloc(sizeof(struct pam_conv), 1);
  pc->conv = conv;
  pc->appdata_ptr = pw;

  e = pam_start("login", user, pc, &ph);
  if (e != PAM_SUCCESS)
    return -1;
  e = pam_authenticate(ph, 0);
  pam_end(ph, 0);
  if (e != PAM_SUCCESS)
    return -1;

  return 0;
}
