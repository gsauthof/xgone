/*
 * Lockscreen for X windows
 * Tim Morgan 2/25/87
 * 
 * Modified for X11 by Richard Johnson
 * Modified/Rewritten extensively afterwards.
 */

#include <X11/Xlib.h>
#include <X11/Xos.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>

#include <pwd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "auth.h"

#define	SLEEP_TIME	3		/* Seconds, of course */
#define FONT           "fixed"         /* BORING! (but always there) */
#define SEPARATION     2               /* ... between lines */
#define DELAYTIME      2               /* delay time for "-delay" option */
#define FALSE          0
#define TRUE           1
#define UTMP		"/var/run/utmp"	/* the utmp file */
#define RESPBUFSIZ    100             /* size of response from user */
#define control(a)     ((a) - ('@'))   /* control characters */

char   DspMsg[1024]="X Gone";  /* Default message to display */
char    DspBlah[100];
char	Dsptime[100];	                /* Time command was started */
char    Dspctime[100];
char    DspBlub[100];
int    fontheight;                     /* height of one line of text */
Pixmap textpixmap;                     /* pixmap to hold text */
int    textwidth, textheight;          /* sizes of whole text area */
int    Sleeptime=SLEEP_TIME;           /* sleep time between updates */
int    Delay=FALSE;                    /* Delay startup (for twm) */
int    lastx=0, lasty=0;               /* last coord's of text */
fd_set select_mask, sel_mask;          /* for select stuff */
int    select_bits;                    /* width of select_mask */
struct timeval sleeptime;              /* for selects */



void entertain(Display *dpy, Window wm, GC gc);
int rnd(int n);

int main(int argc, char **argv)
{
    Display                    *dpy;
    XSetWindowAttributes       xswa;
    Visual                     visual;
    Window                     w;
    GC                         gc;
    XGCValues                  gcvalues;
    XEvent                     in;
    XFontStruct                        *font;
    XColor                     blackcolor = {0};
    XColor                     whitecolor = {0};
    Pixmap                     pixmap;
    Cursor                     cursor;
    char                      buffer[100], inputbuf[80], *string, *user_name,
                              response[RESPBUFSIZ];
    char			*pw;
    int                                len, password_bad = 1, response_i, nbytes, i;
    struct passwd              *u;
    char                       *myname, *dspptr;
    int                                width, lenwidth;
    int                                timeout, interval,
                               preference, allowexp;
    static char                        fontname[80]={FONT};

    time_t t;
    unsigned long l;
    int quit = 0;
    int debug = 0;

    if((myname = rindex(argv[0], '/')) == NULL)
       myname = argv[0];
    else
       myname++;

    if (!(dpy = XOpenDisplay(NULL))) {
       fprintf(stderr, "%s: Can't open display '%s'\n",
		myname, XDisplayName(NULL));
        return 1;
    }
/*
 * Get the defaults
 */
    if((string = XGetDefault(dpy, myname, "Font")) != NULL)
       strcpy(fontname, string);
    if((string = XGetDefault(dpy, myname, "Text")) != NULL)
       strcpy(DspMsg, string);
    if((string = XGetDefault(dpy, myname, "Sleep")) != NULL)
       Sleeptime = atoi(string);
    if((string = XGetDefault(dpy, myname, "Update")) != NULL)
       Sleeptime = atoi(string);
    if((string = XGetDefault(dpy, myname, "Delay")) != NULL) {
       if(strcmp(string, "off") == 0 || strcmp(string, "0") == 0
          || strcmp(string, "no") == 0)
           Delay = FALSE;
       else
           Delay = TRUE;
    }
           
/*
 * Parse arguments
 */
    for(--argc , ++argv ; *argv && **argv == '-' ; argv++ , argc--) {
       ++*argv;
       if(strcmp(*argv, "fn") == 0 || strncmp(*argv, "font", 1) == 0)
           strcpy(fontname, *++argv);  /* save font name */
       else if(strncmp(*argv, "sleep", 1) == 0
          || strncmp(*argv, "update", 1) == 0)
           Sleeptime = atoi(*++argv);
       else if(strncmp(*argv, "delay", 1) == 0)
           Delay = TRUE;
       else if(strncmp(*argv, "test", 1) == 0)
           debug = 1;
    }

/*
 * If we should delay startup, then fork and exit the parent right now.
 * (This way, "twm" changes the cursor back to the original one and lets
 *  go of the mouse so that we can grab it.)
 */
    if(Delay)
       switch(fork()) {
       case 0: /* child  -  continue with program */
           sleep(DELAYTIME);
           break;

       case -1:        /* error during fork  -  exit */
           fprintf(stderr, "Can't fork - ");
           perror("");
           exit(1);

       default:        /* parent  -  exit */
           exit(0);
       }

/*
 * Get the message
 */
    dspptr = DspMsg;
    if( *argv ) strcpy(dspptr, "\0");
    while( *argv ) {
       strcat(dspptr, *argv++);
       strcat(dspptr, " ");
    }

/*
 * Set random number generator
 */
    srandom(time(0));

/*
 * Make a big window to cover the screen
 */
    xswa.background_pixel = BlackPixel(dpy, DefaultScreen(dpy));
    xswa.override_redirect = True;
    visual.visualid = CopyFromParent;
    w = XCreateWindow(dpy, DefaultRootWindow(dpy), 0, 0, 9999, 9999,
                     0, DefaultDepth(dpy, DefaultScreen(dpy)), InputOutput,
               &visual, CWBackPixel | CWOverrideRedirect, &xswa);

/*
 * Get their chosen font information
 */
    font = XLoadQueryFont(dpy, fontname);
    fontheight = font->max_bounds.ascent
       + font->max_bounds.descent + SEPARATION;

/*
 * Make sure we get the events we want
 */
    l = ButtonPressMask | ExposureMask | KeyPressMask | VisibilityChangeMask;
    XSelectInput(dpy, w, l);

/*
 * The infamous GC!  (Oh no!!  Not the *GC*!!!  ARRRGGHHH!! :-)   )
 */
    gcvalues.font = font->fid;
    gcvalues.fill_style = FillOpaqueStippled;
    gcvalues.foreground = WhitePixel(dpy, DefaultScreen(dpy));
    gcvalues.background = BlackPixel(dpy, DefaultScreen(dpy));
    gc = XCreateGC(dpy, w, GCFont|GCFillStyle|GCForeground|GCBackground
                  , &gcvalues);

    XMapWindow(dpy, w);                /* put it up on the screen       */

/*
 * Get rid of the cursor
 */
    pixmap = XCreatePixmap(dpy, w, 1, 1
               , DefaultDepth(dpy, DefaultScreen(dpy)));
    XSetForeground(dpy, gc, BlackPixel(dpy, DefaultScreen(dpy)));
    XFillRectangle(dpy, pixmap, gc, 0, 0, 1, 1);
    XSetForeground(dpy, gc, WhitePixel(dpy, DefaultScreen(dpy)));
    blackcolor.pixel = BlackPixel(dpy, DefaultScreen(dpy));
    whitecolor.pixel = WhitePixel(dpy, DefaultScreen(dpy));
    cursor = XCreatePixmapCursor(dpy, pixmap, None,
       &blackcolor, &whitecolor, 0, 0);
    XDefineCursor(dpy, w, cursor);

/*
 * Grab mouse and keyboard at same time
 */
    i = XGrabKeyboard(dpy, w, True, GrabModeAsync, GrabModeAsync, CurrentTime);
    if (i != GrabSuccess) {
	fprintf(stderr, "Can't grab keyboard\n");
        return 1;
    }
    i = XGrabPointer(dpy, w, False, ButtonPressMask, GrabModeAsync,
       GrabModeAsync,
	w, cursor, CurrentTime);
    if (i != GrabSuccess) {
       fprintf(stderr, "Can't grab mouse\n");
	return 1;
    }

/*
 * Turn off the screen saver control to make sure they see our window
 * (But make sure you can turn it back on again in the right way!)
 */
    XGetScreenSaver(dpy, &timeout, &interval, &preference, &allowexp);
    XSetScreenSaver(dpy, 0, interval, preference, allowexp);

    u = getpwuid(getuid());
    if (!u) {
	perror("Who are you?");
        return 1;
    }
    user_name = u->pw_name;

    sprintf(buffer, "Enter password for user %s:", user_name);
    len = strlen(buffer);
    lenwidth = XTextWidth(font, buffer, len);
/*
 * What time is it anyway?  *That late*!  It's time to go home!
 * (or start an FTP!)
 */
    time(&t);
    strcat(Dsptime, ctime(&t));
    Dsptime[strlen(Dsptime)-1] = '\0'; /* remove the '\n' on the end */
    sprintf(DspBlah, "Started by %-8s at %s", user_name, Dsptime);
/*
 * Now to save time (we all need to do *that*, right?) we create a pixmap
 * and put the text into it.  That way we can simply do a CopyArea to the
 * right place later.
 */
    textheight = fontheight * 2 + SEPARATION;
    textwidth = XTextWidth(font, DspMsg, strlen(DspMsg));
    if(( i = XTextWidth(font, DspBlah, strlen(DspBlah))) > textwidth)
       textwidth = i;
    textpixmap = XCreatePixmap(dpy, w, textwidth, textheight
               , DefaultDepth(dpy, DefaultScreen(dpy)));
    XSetForeground(dpy, gc, BlackPixel(dpy, DefaultScreen(dpy)));
    XFillRectangle(dpy, textpixmap, gc, 0, 0, textwidth, textheight);
    XSetBackground(dpy, gc, BlackPixel(dpy, DefaultScreen(dpy)));
    XSetForeground(dpy, gc, WhitePixel(dpy, DefaultScreen(dpy)));
    XDrawString(dpy, textpixmap, gc, 0, fontheight, DspMsg, strlen(DspMsg));
    XDrawString(dpy, textpixmap, gc, 0, fontheight*2, DspBlah
		, strlen(DspBlah));

/*
 * Lost forever in the infinite loop...
 */
    do {
       entertain(dpy, w, gc);
       XNextEvent(dpy, &in);

        switch (in.type) {
          case ButtonPress:
            if (debug)
              return 23;
            break;
          case KeyPress:
            break;
          default:
            continue;
        }

       /*
        * Clear off the place where we drew the info. string last time
        */
	XClearArea(dpy, w, lastx, lasty, textwidth, textheight + fontheight
            + SEPARATION, 0);

       /*
        * Prompt them for the password
        */
       XDrawString(dpy, w, gc, 100, 100, buffer, len);
       XFlush(dpy);
       response_i = 1;
       response[0] = 'x';      /* filler */
        quit = 0;
           /* Gather password */
	for (;;) {
           if (in.type == KeyPress) {
	        nbytes = XLookupString(&(in.xkey), inputbuf, sizeof(inputbuf), 0, 0);

                for (i = 0; i < nbytes; i++)
                  switch (inputbuf[i]) {
                    case control('H') :
                      if (response_i > 1)
                        response_i--;
                      break;
                    case control('U') :
                      response_i = 1;
                      break;
                    default:
                      if (response_i < RESPBUFSIZ)
                        response[response_i++] = inputbuf[i];
                      else
                        quit = 1;
               }
           }

                
            if ((response[response_i - 1] == '\r') || quit)
              break;
           XNextEvent(dpy, &in);
       }

       response[response_i - 1] = '\0';
        pw = response + 1;
        
        password_bad = authenticate(user_name, pw);
       if (password_bad) {
           if (password_bad) {
               width = XTextWidth(font, "Sorry", 5);
               XDrawString(dpy, w, gc, 100, 300, "Sorry", 5);
               XFlush(dpy);
               sleep(Sleeptime);
               XClearArea(dpy, w, 100, 300 - font->max_bounds.ascent, width
                          , textheight, 0);
               XFlush(dpy);
           }
       }
       XClearArea(dpy, w, 100, 100 - font->max_bounds.ascent, lenwidth
                  , textheight, 0);
       XFlush(dpy);
    } while (password_bad);

    XUngrabPointer(dpy, CurrentTime);
    XDestroyWindow(dpy, w);            /* throw it away */

/*
 * Set screen saver back the way it was
 */
    XSetScreenSaver(dpy, timeout, interval, preference, allowexp);

    XFlush(dpy);                       /* and make sure the server sees it */

    exit(0);
}

/* Do something entertaining on the screen */
void entertain(Display *dpy, Window w, GC gc)
{
    int x, y;
    time_t t;

    x = 0; y = 0;
    while (XPending(dpy) == 0) {
       /* generate rand. numbers such that our text will be on the screen */
       x = rnd(DisplayWidth(dpy, DefaultScreen(dpy)) - textwidth);
	y = rnd(DisplayHeight(dpy, DefaultScreen(dpy)) - textheight
            - fontheight - SEPARATION);
       /* bye, bye, history! */
	XClearArea(dpy, w, lastx, lasty, textwidth, textheight + fontheight
            + SEPARATION, 0);
       /* I like being on top! */
       XRaiseWindow(dpy, w);
       /* ok, let's tell everyone what's happening */
       XCopyArea(dpy, textpixmap, w, gc, 0, 0, textwidth, textheight, x, y);

        time(&t);
        strcpy(Dspctime, ctime(&t));
        Dspctime[strlen(Dspctime)-1] = '\0';
        sprintf(DspBlub, "Current time:          %s", Dspctime);
	XDrawString(dpy, w, gc, x, y + textheight + fontheight, DspBlub,
            strlen(DspBlub));
        
       lastx = x;
       lasty = y;
       /* NOW! */
       XFlush(dpy);
       /* You've done well, go to bed now.   (With whom?) */
       sleep(Sleeptime);
    }
}

/*
 * Pick a number between 0 and n-1
 */
int rnd(int n)
{
    return random() % n;
    }

