## Original description ##

     Xgone locks the screen until the current user's password  is
     entered.  While waiting, it displays a message telling where
     the person went and when they started the program and  moves
     this  text  around  the screen to avoid burning them in at a
     particular location.

## Improvements ##

* Applied two classic Usenet patches to the original Usenet source from 1990
* Fixed the source code for compilation and use on current Linux
  distributions, Unices, X11 distributions and C-compilers
* Include more information in the status message
* Added pam-support for authentication
* Removed security by obscurity feature

2011-09-22

## Install ##

    $ make
    $ cp xgone $YOUR_PREFIX/bin

## Feedback ##

I appreciate feedback and comments:

    mail@georg.so

## Solaris notes ##

Didn't test it on many Solaris system. Just on one x86 Solaris 10 box. At this
machine the pam service 'login' resulted in a subsystem error. But 'rlogin'
worked.

Don't know if this is a local or generic problem of Solaris.

You can test different pam services with the supplied utility:

    $ ./pam rlogin

Thus, if you have the same problem you have to change the service name
in `auth.c`.


